package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.UpdatePlayerMoveStateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleFall extends Module {

  public ModuleFall() {
    super("Fall", ModuleCategory.MOVEMENT);

    this.setVersion("1.0.1");
    this.setBuildVersion(18201);
    this.setDescription("Makes you fall/tp down.");
    this.setRegistered(true);
  }
  
  @EventHandler
  public void onUpdateMoveState(UpdatePlayerMoveStateEvent updateEvent) {
    if (Wrapper.getPlayer().motionY < 0) {
      if(!Wrapper.getPlayer().isInWater()){
        Wrapper.getPlayer().motionY = -100.0D;
      }
      if (Wrapper.getPlayer().isInWater() && Wrapper.getGameSettings().keyBindSneak.isKeyDown()) {
        Wrapper.getPlayer().motionY = -100.0D;
      }
    }
  }
}
